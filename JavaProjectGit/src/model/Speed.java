package model;

public class Speed {
	
	public static Double convertToKmPerHour(Double meterPerSecond) {
		return meterPerSecond * 3.6;
	}
	
	public static Double convertToMetersPerSecond(Double kmPerHour) {
		return kmPerHour / 3.6;
	}

}
