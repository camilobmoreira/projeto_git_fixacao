package model;

public class Temperature {
	
	public static Double convertToCelsius(Double farenheit) {
		return (farenheit - 32.0) * (5.0/9.0);
	}
	
	public static Double convertToFarenheit(Double celsius) {
		return celsius * (9.0/5.0) + 32.0;
	}

}
