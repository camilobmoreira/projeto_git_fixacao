package model;

public class Money {

	public static Double convertToReal(Double dollar) {
		return dollar * 3.17;
	}

	public static Double convertToDollar(Double real) {
		return real / 3.17;
	}

}
