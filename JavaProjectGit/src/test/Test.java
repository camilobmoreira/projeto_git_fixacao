package test;

import java.text.DecimalFormat;

import model.Money;
import model.Speed;
import model.Temperature;

public class Test {

	public static void main(String[] args) {
		
		System.out.println("Converting 37.00 real to dollar");
		System.out.println("US$" +  new DecimalFormat("###,##0.00").format(Money.convertToDollar(37.00)));
		System.out.println("\n");
		
		System.out.println("Converting 25.00 dollars to real:");
		System.out.println("R$" +  new DecimalFormat("###,##0.00").format(Money.convertToReal(25.00)));
		System.out.println("\n");

		System.out.println("**********\n");
		
		System.out.println("Converting 45.0 m/s to km/h");
		System.out.println(new DecimalFormat("###,##0.00").format(Speed.convertToKmPerHour(45.0)) + "km/h");
		System.out.println("\n");
		
		System.out.println("Converting 80.0 km/h to m/s");
		System.out.println(new DecimalFormat("###,##0.00").format(Speed.convertToMetersPerSecond(88.0)) + " m/s" );
		System.out.println("\n");
		
		System.out.println("**********\n");
		
		System.out.println("Converting 57.0 degrees farenheit to celsius");
		System.out.println(new DecimalFormat("###,##0.00").format(Temperature.convertToCelsius(57.0)) + "ºC");
		System.out.println("\n");
		
		System.out.println("Converting 120.0 degrees celsius to farenheit");
		System.out.println(new DecimalFormat("###,##0.00").format(Temperature.convertToFarenheit(120.0)) + "ºF");
		System.out.println("\n");
	}

}
